#include <stdio.h>
#include <iostream> 
#include <iomanip>
#include <string.h>
#include <stdlib.h>



int main()
{

	FILE* f;
	long int i;
	long double m, k;
	char* rownanie = new char[128]; //= &row[0];
	std::cout << "Wpisz liczbe" << std::endl;
	std::cin >> i;

	m = i * 0.3; //stopy na metry
	k = i * 1.609344; //mile na kilometry


#pragma warning(suppress : 4996);
	f = fopen("lab.txt", "w");
	if (f == NULL)
	{
		perror(" Nie udalo sie otworzyc pliku 'lab1.txt' ");
		return -1;
	}

	std::cout << i << " stopy to " << m << " metrow" << std::endl;
	fprintf(f, "%d stopy to %lf metr�w\n", i, m);
	printf("\a"); //dzwi�k
	std::cout << i << " mil to " << k << " kilometrow" << std::endl;
	fprintf(f, "%d mil to %lf kilometr�w\n", i, k);

	if (i % 2 == 0) {
		std::cout << "Liczba " << i << " jest parzysta" << std::endl;
		fprintf(f, "Liczba %d jest parzysta\n", i);
	}
	else
	{
		std::cout << "Liczba " << i << " jest nieparzysta" << std::endl;
		fprintf(f, "Liczba %d jest nieparzysta\n", i);
	}

	if (i > 0) {
		std::cout << "Liczba " << i << " jest wieksza od zera" << std::endl;
		fprintf(f, "Liczba %d jest wieksza od zera\n", i);
	}
	else if (i < 0) {
		std::cout << "Liczba " << i << " jest mniejsza od zera" << std::endl;
		fprintf(f, "Liczba %d jest mniejsza od zera\n", i);
	}
	else {
		std::cout << "Liczba " << i << " jest rowna zeru" << std::endl;
		fprintf(f, "Liczba %d jest rowna zeru ", i);
	}


	std::cout << "Rownanie do rozwiazania (dwie zmienne - np. a*b)" << std::endl;
	std::cin >> rownanie;
	int dl = 0;
	dl = strlen(rownanie);


	char oper = 'a';
	char* tmpNum1 = new char[20];
	memset(tmpNum1, 0, sizeof(tmpNum1));

	int tmpNum[2];
	int j = 0;
	int l = 0;
	int b = 0;
	int hej = 0;

	for (j; j < dl; j++) {
		if (rownanie[j] == '/' || rownanie[j] == '*' || rownanie[j] == '+' || rownanie[j] == '-') {
			oper = rownanie[j];
			memcpy(tmpNum1, rownanie + hej, j);
			hej = j + 1;
			tmpNum[l++] = atoi(tmpNum1);
		}
	}
	memcpy(tmpNum1, rownanie + hej, j);
	tmpNum[l++] = atoi(tmpNum1);



	long double wynik = 0;
	if (oper == 'a') {
		std::cout << "Bledne rownanie"<<std::endl;
		fprintf(f, "Bledne rownanie\n");
	}
	else {
		switch (oper)
		{
		case '/':
			wynik = (double)tmpNum[0] / (double)tmpNum[1];
			break;
		case '+':
			wynik = tmpNum[0] + tmpNum[1];
			break;
		case '-':
			wynik = tmpNum[0] - tmpNum[1];
			break;
		case '*':
			wynik = tmpNum[0] * tmpNum[1];
			break;
		default:
			break;
		}
	}
	std::cout << "Wynik " << wynik << std::endl << std::endl;
	fprintf(f, "R�wnanie (%d %c %d) \n", tmpNum[0], oper, tmpNum[1]);
	fprintf(f, "Wynik: %lf\n", wynik);

	long a, p, h, v;
	std::cout << "Obliczanie objetosci rownolegloboku o podstawie prostokata.\n";
	std::cout << "Podaj pierwszy bok podstawy: \n";
	std::cin >> a;
	std::cout << "Podaj drugi bok podstawy: \n";
	std::cin >> p;
	std::cout << "Podaj wysokosc rownolegloboku: \n";
	std::cin >> h;
	v = a * p * h;
	printf("Objetosc wynosi: %ld", v);
	fprintf(f, "Obliczanie obj�to�ci ze wzoru: V = a * b * h \n V = %ld * %ld * %ld \n V = %ld", a, p, h, v);

	fflush(f);
	fclose(f);
	return 0;

}

